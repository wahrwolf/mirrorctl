use crate::modules::mirror::Mirror;
use crate::url_handler::try_build_url_from_path_buf;
use chrono::{DateTime, TimeZone, Utc};
use std::collections::{HashMap, HashSet};
use std::path::PathBuf;
use url::Url;

use super::manifest::*;
mod manifest;

use super::registry::*;
mod registry;

use super::entities::*;
mod entities;

use super::usecases::*;
mod usecases;

fn get_test_mirror_name() -> String {
    "mirror.example.org".to_string()
}

fn get_url_for_test_mirror() -> Url {
    let target_file = PathBuf::from("/foo/bar.toml");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    url
}

fn build_test_mirror() -> Mirror {
    Mirror::new(get_test_mirror_name())
}

fn get_uptime_events() -> HashSet<DateTime<Utc>> {
    let mut set = HashSet::new();
    set.insert(get_datetime_for_registered_uptime_event());
    set.insert(get_datetime_for_unregistered_uptime_event());
    set
}

fn get_datetime_for_registered_downtime_event() -> DateTime<Utc> {
    Utc.with_ymd_and_hms(2020, 4, 24, 12, 0, 0).unwrap()
}

fn get_datetime_for_registered_uptime_event() -> DateTime<Utc> {
    Utc.with_ymd_and_hms(2020, 12, 25, 0, 0, 0).unwrap()
}

fn get_datetime_for_unregistered_downtime_event() -> DateTime<Utc> {
    Utc.with_ymd_and_hms(2020, 1, 24, 0, 0, 0).unwrap()
}

fn get_datetime_for_unregistered_uptime_event() -> DateTime<Utc> {
    Utc.with_ymd_and_hms(2001, 3, 22, 0, 0, 0).unwrap()
}

fn build_registered_event_without_downtime() -> MaintenanceEvent {
    MaintenanceEvent {
        description: "Registered Event without downtime".to_string(),
        note: None,
        is_downtime: false,
        starts_at: Utc.with_ymd_and_hms(2020, 12, 20, 0, 0, 0).unwrap(),
        ends_at: Utc.with_ymd_and_hms(2021, 1, 7, 23, 59, 59).unwrap(),
    }
}

fn build_registered_event_with_downtime() -> MaintenanceEvent {
    MaintenanceEvent {
        description: "Registered Event with downtime".to_string(),
        note: None,
        is_downtime: true,
        starts_at: Utc.with_ymd_and_hms(2020, 4, 24, 0, 0, 0).unwrap(),
        ends_at: Utc.with_ymd_and_hms(2020, 4, 25, 0, 0, 0).unwrap(),
    }
}

fn get_registered_events_as_set() -> HashSet<MaintenanceEvent> {
    let mut set: HashSet<MaintenanceEvent> = HashSet::new();
    set.insert(build_registered_event_with_downtime());
    set.insert(build_registered_event_without_downtime());
    set
}

fn get_registered_events_as_map_with_mirror_name(
) -> HashMap<Option<String>, HashSet<MaintenanceEvent>> {
    let mut map = HashMap::new();
    map.insert(Some(get_test_mirror_name()), get_registered_events_as_set());
    map
}

fn get_registered_events_as_map_with_url() -> HashMap<Option<String>, HashSet<MaintenanceEvent>> {
    let mut map = HashMap::new();
    map.insert(None, get_registered_events_as_set());
    map
}

fn build_unregistered_event_with_downtime() -> MaintenanceEvent {
    MaintenanceEvent {
        description: "Unregistered Event with downtime".to_string(),
        note: None,
        is_downtime: true,
        starts_at: Utc.with_ymd_and_hms(2020, 1, 23, 0, 0, 0).unwrap(),
        ends_at: Utc.with_ymd_and_hms(2020, 2, 22, 0, 0, 0).unwrap(),
    }
}

fn build_unregistered_event_without_downtime() -> MaintenanceEvent {
    MaintenanceEvent {
        description: "Unregistered Event without downtime".to_string(),
        note: None,
        is_downtime: false,
        starts_at: Utc.with_ymd_and_hms(2001, 3, 21, 0, 0, 0).unwrap(),
        ends_at: Utc.with_ymd_and_hms(2002, 1, 11, 0, 0, 0).unwrap(),
    }
}

fn build_registry_without_events() -> MaintenanceEventRegistry {
    let registry = MaintenanceEventRegistry::default();
    registry
}

fn build_registry_with_event_without_mirror_names_with_url() -> MaintenanceEventRegistry {
    let mut registry = MaintenanceEventRegistry::default();
    registry.register_event_with_url(
        &get_url_for_test_mirror(),
        build_registered_event_with_downtime(),
    );
    registry.register_event_with_url(
        &get_url_for_test_mirror(),
        build_registered_event_without_downtime(),
    );
    registry
}

fn build_registry_with_event_with_mirror_names_without_url() -> MaintenanceEventRegistry {
    let mut registry = MaintenanceEventRegistry::default();
    registry.register_event_with_mirror_name(
        &get_test_mirror_name(),
        build_registered_event_with_downtime(),
    );
    registry.register_event_with_mirror_name(
        &get_test_mirror_name(),
        build_registered_event_without_downtime(),
    );
    registry
}

fn build_test_manifest_without_events() -> MaintenanceManifest {
    let mut manifest = MaintenanceManifest::default();
    manifest.last_updated = Utc.with_ymd_and_hms(2024, 5, 4, 0, 0, 0).unwrap();
    manifest
}

fn build_test_manifest_with_events_without_mirror_names() -> MaintenanceManifest {
    let mut manifest = build_test_manifest_without_events();
    manifest.insert_event(None, build_registered_event_without_downtime());
    manifest.insert_event(None, build_registered_event_with_downtime());
    manifest
}

fn build_test_manifest_with_events_with_mirror_names() -> MaintenanceManifest {
    let mut manifest = build_test_manifest_without_events();
    manifest.insert_event(
        Some(get_test_mirror_name()),
        build_registered_event_without_downtime(),
    );
    manifest.insert_event(
        Some(get_test_mirror_name()),
        build_registered_event_with_downtime(),
    );
    manifest
}
