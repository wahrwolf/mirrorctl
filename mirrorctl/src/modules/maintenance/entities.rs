use chrono::{DateTime, Days, Utc};
use serde::{Deserialize, Serialize};
use std::hash::{Hash, Hasher};

#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct MaintenanceEvent {
    pub description: String,
    pub note: Option<String>,
    pub is_downtime: bool,
    pub starts_at: DateTime<Utc>,
    pub ends_at: DateTime<Utc>,
}

impl MaintenanceEvent {
    pub fn new(description: String) -> MaintenanceEvent {
        MaintenanceEvent {
            description: description.clone(),
            note: None,
            is_downtime: true,
            starts_at: Utc::now(),
            ends_at: Utc::now() + Days::new(1),
        }
    }
    pub fn new_with_args(
        description: String,
        note: Option<String>,
        is_downtime: bool,
        starts_at: DateTime<Utc>,
        ends_at: DateTime<Utc>,
    ) -> MaintenanceEvent {
        MaintenanceEvent {
            description: description.clone(),
            note: note.clone(),
            is_downtime: is_downtime.clone(),
            starts_at: starts_at.clone(),
            ends_at: ends_at.clone(),
        }
    }
}

impl Hash for MaintenanceEvent {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.description.hash(state);
    }
}

impl PartialEq for MaintenanceEvent {
    fn eq(&self, other: &Self) -> bool {
        self.description == other.description
    }
}

impl Eq for MaintenanceEvent {}
