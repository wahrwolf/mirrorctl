use crate::url_handler::fetch_string_from_url;
use anyhow::{Context, Result};
use clap::Args;
use log::info;
use std::collections::HashSet;
use std::path::PathBuf;
use url::Url;

use crate::modules::maintenance::MaintenanceManifest;
use crate::url_handler::{build_record_from_url, push_record_to_url};

use crate::cli::{edit_record, edit_record_from_url, ProgramState};
use crate::modules::maintenance::{
    fetch_maintenance_manifest_for_mirror_from_url, get_default_maintenance_manifest_url,
    push_maintenance_manifest_for_mirror_to_url, push_maintenance_manifest_to_mirror_directory,
    sync_maintenance_manifest_for_mirror_to_url, sync_maintenance_manifest_to_mirror_directory,
    sync_maintenance_manifests_for_mirror,
};
use crate::modules::mirror::{
    fetch_or_create_mirror_from_registry, register_maintenance_url_for_mirror,
    sync_mirror_manifest_to_mirror_directory,
};
use crate::url_handler::try_build_url_from_path_buf;

#[derive(Debug, Args)]
pub struct CatManifestArgs {
    mirror: Option<String>,
    #[arg(short, long)]
    url: Option<Url>,
}

pub fn cat_manifest(args: CatManifestArgs, mut state: ProgramState) -> Result<()> {
    let url: Url = match args.url {
        Some(url) => url,
        None => {
            let mirror_name = args
                .mirror
                .context("Neither --url nor --mirror set. Specifiy one of them!")?;
            let mirror = fetch_or_create_mirror_from_registry(mirror_name, &mut state.mirrors)?;
            get_default_maintenance_manifest_url(&mirror, &state.base_directory)?
        }
    };
    let record = fetch_string_from_url(&url, &state.protocol_handlers)?;
    match record {
        Some(record) => {
            println!("{}", record);
        }
        None => info!("Mainfest exists, but is empty!"),
    }
    Ok(())
}

#[derive(Debug, Args)]
pub struct ListManifestArgs {
    mirror: Option<String>,
}

pub fn list_manifests(args: ListManifestArgs, mut state: ProgramState) -> Result<()> {
    let urls = match args.mirror {
        Some(mirror) => {
            let mirror = fetch_or_create_mirror_from_registry(mirror, &mut state.mirrors)?;
            let mut urls: HashSet<Url> = HashSet::default();
            for url in mirror.get_maintenance_urls() {
                urls.insert(url.clone());
            }
            for url in state.events.get_urls_for_mirror_name(&mirror.name) {
                urls.insert(url.clone());
            }
            urls
        }
        None => state.events.get_registered_urls(),
    };
    for url in urls {
        println!("{}", url);
    }
    Ok(())
}

#[derive(Debug, Args)]
pub struct CreateManifestArgs {
    mirror: String,
    #[arg(short, long)]
    url: Option<Url>,
}

pub fn create_manifest_for_mirror(args: CreateManifestArgs, mut state: ProgramState) -> Result<()> {
    let mirror = fetch_or_create_mirror_from_registry(args.mirror, &mut state.mirrors)?;
    let url: Url = match args.url {
        Some(url) => url,
        None => get_default_maintenance_manifest_url(&mirror, &state.base_directory)?,
    };
    sync_maintenance_manifest_for_mirror_to_url(
        mirror,
        &url,
        &state.events,
        &state.protocol_handlers,
        &state.format_handlers,
    )?;
    Ok(())
}

#[derive(Debug, Args)]
pub struct RegisterManifestArgs {
    mirror: String,
    url: Url,
    #[arg(short, long, action)]
    force: bool,
}

pub fn register_manifest_for_mirror(
    args: RegisterManifestArgs,
    mut state: ProgramState,
) -> Result<()> {
    if args.force {
        let _ = fetch_or_create_mirror_from_registry(args.mirror.clone(), &mut state.mirrors)?;
    }
    register_maintenance_url_for_mirror(&args.mirror, &args.url, &mut state.mirrors)?;
    sync_mirror_manifest_to_mirror_directory(
        &args.mirror,
        &state.mirrors,
        &state.base_directory,
        &state.protocol_handlers,
        &state.format_handlers,
    )?;
    Ok(())
}

#[derive(Debug, Args)]
pub struct UpdateManifestArgs {}

pub fn update_manifest(_args: UpdateManifestArgs, _state: ProgramState) -> Result<()> {
    todo!("Can not update manifests yet!");
}

#[derive(Debug, Args)]
pub struct EditManifestArgs {
    mirror: Option<String>,
    #[arg(short, long)]
    url: Option<Url>,
    #[arg(short, long, env)]
    editor: String,
    #[arg(short, long, env, default_value = "toml")]
    format: String,
    #[arg(long, action)]
    force: bool,
}

pub fn edit_manifest(args: EditManifestArgs, mut state: ProgramState) -> Result<()> {
    let url: Url = match args.url {
        Some(url) => url,
        None => {
            let mirror_name = args
                .mirror
                .context("Neither --url nor MIRROR set. Specifiy one of them!")?;
            let mirror = fetch_or_create_mirror_from_registry(mirror_name, &mut state.mirrors)?;
            get_default_maintenance_manifest_url(&mirror, &state.base_directory)?
        }
    };

    if args.force {
        edit_record_from_url(args.editor, &url, args.format, &state.protocol_handlers)?;
    } else {
        let mut manifest: MaintenanceManifest =
            build_record_from_url(&url, &state.protocol_handlers, &state.format_handlers)?;
        manifest = edit_record(
            args.editor,
            manifest,
            args.format,
            &state.protocol_handlers,
            &state.format_handlers,
        )?;
        push_record_to_url(
            &url,
            &manifest,
            &state.protocol_handlers,
            &state.format_handlers,
        )?;
    }
    Ok(())
}

#[derive(Debug, Args)]
pub struct PushManifestArgs {
    #[arg(short, long)]
    mirror: String,
    #[arg(short, long)]
    url: Option<Url>,
    #[arg(short, long)]
    file: Option<PathBuf>,
}

pub fn push_manifest_to_url(args: PushManifestArgs, mut state: ProgramState) -> Result<()> {
    let url: Url = match args.file {
        None => args
            .url
            .context("Neither --url nor --file set. Specifiy one of them!")?,
        Some(path) => try_build_url_from_path_buf(&path)?,
    };

    let mirror = fetch_or_create_mirror_from_registry(args.mirror, &mut state.mirrors)?;
    push_maintenance_manifest_for_mirror_to_url(
        mirror,
        &url,
        &state.events,
        &state.protocol_handlers,
        &state.format_handlers,
    )?;
    Ok(())
}

#[derive(Debug, Args)]
pub struct FetchManifestArgs {
    #[arg(short, long)]
    mirror: String,
    #[arg(short, long)]
    url: Option<Url>,
    #[arg(short, long)]
    file: Option<PathBuf>,
}

pub fn fetch_manifest_from_url(args: FetchManifestArgs, mut state: ProgramState) -> Result<()> {
    let url: Url = match args.file {
        None => args
            .url
            .context("Neither --url nor --file set. Specifiy one of them!")?,
        Some(path) => try_build_url_from_path_buf(&path)?,
    };
    let mirror = fetch_or_create_mirror_from_registry(args.mirror, &mut state.mirrors)?;
    fetch_maintenance_manifest_for_mirror_from_url(
        &mirror.name,
        &url,
        &mut state.events,
        &state.protocol_handlers,
        &state.format_handlers,
    )?;
    sync_maintenance_manifest_to_mirror_directory(
        &mirror,
        &state.events,
        &state.base_directory,
        &state.protocol_handlers,
        &state.format_handlers,
    )?;
    Ok(())
}

#[derive(Debug, Args)]
pub struct SyncManifestArgs {
    #[arg(short, long)]
    mirror: String,
}

pub fn sync_manifests(args: SyncManifestArgs, mut state: ProgramState) -> Result<()> {
    let mirror = fetch_or_create_mirror_from_registry(args.mirror, &mut state.mirrors)?;
    sync_maintenance_manifests_for_mirror(
        &mirror,
        &state.protocol_handlers,
        &state.format_handlers,
    )?;
    Ok(())
}

#[derive(Debug, Args)]
pub struct CleanupArgs {
    #[arg(short, long)]
    mirror: Option<String>,
}

pub fn cleanup_local_manifests(args: CleanupArgs, mut state: ProgramState) -> Result<()> {
    let mirror_names = match args.mirror {
        None => state.events.get_registered_mirror_names(),
        Some(mirror_name) => {
            let mut mirror_names = HashSet::default();
            mirror_names.insert(mirror_name);
            mirror_names
        }
    };
    for mirror_name in mirror_names {
        let mirror = fetch_or_create_mirror_from_registry(mirror_name, &mut state.mirrors)?;
        push_maintenance_manifest_to_mirror_directory(
            &mirror,
            &state.events,
            &state.base_directory,
            &state.protocol_handlers,
            &state.format_handlers,
        )?;
    }
    Ok(())
}
