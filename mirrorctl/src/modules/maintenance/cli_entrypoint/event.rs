use anyhow::Result;
use clap::Args;
use humantime::Timestamp;
use std::collections::HashSet;

use crate::cli::{edit_record, transform_humantime, ProgramState};
use crate::modules::maintenance::{
    create_maintenance_event_for_mirror, delete_maintenance_event_for_mirror,
    edit_maintenance_event_for_mirror, extract_maintenance_event_for_mirror,
    push_maintenance_manifest_to_mirror_directory, register_maintenance_event_for_mirror,
    sync_maintenance_manifest_to_mirror_directory,
};
use crate::modules::mirror::fetch_or_create_mirror_from_registry;

#[derive(Debug, Args)]
pub struct ShowEventArgs {
    mirror: String,
    description: String,
}

pub fn show_event(args: ShowEventArgs, mut state: ProgramState) -> Result<()> {
    let event = state
        .events
        .get_event_by_mirror_name(&args.mirror, &args.description)
        .expect("Could not find event!");
    println!("Maintenance Event at: {}/{}", args.mirror, args.description);
    println!("Description: {}", event.description);
    println!("starts_at: {}", event.starts_at);
    println!("ends_at: {}", event.ends_at);
    print!("Note:");
    match &event.note {
        Some(note) => println!(" {}", note),
        None => println!("<EMPTY>"),
    }

    Ok(())
}

#[derive(Debug, Args)]
pub struct ListEventArgs {
    mirror: Option<String>,
}

pub fn list_events(args: ListEventArgs, state: ProgramState) -> Result<()> {
    let mirror_names = match args.mirror {
        None => state.events.get_registered_mirror_names(),
        Some(mirror_name) => {
            let mut mirror_names = HashSet::default();
            mirror_names.insert(mirror_name);
            mirror_names
        }
    };
    for mirror_name in mirror_names {
        println!("{}:", &mirror_name);
        let events = state.events.get_events_by_mirror_name(&mirror_name);
        for event in events {
            println!(
                "- {} ({} - {})",
                event.description, event.starts_at, event.ends_at
            );
        }
    }
    Ok(())
}

#[derive(Debug, Args)]
pub struct CreateEventArgs {
    mirror: String,
    description: String,
    #[arg(short, long)]
    from: Timestamp,
    #[arg(short, long)]
    until: Timestamp,
    #[arg(short, long)]
    is_downtime: bool,
    #[arg(short, long)]
    note: Option<String>,
}

pub fn create_event(args: CreateEventArgs, mut state: ProgramState) -> Result<()> {
    let mirror = fetch_or_create_mirror_from_registry(args.mirror, &mut state.mirrors)?;
    create_maintenance_event_for_mirror(
        transform_humantime(args.from),
        transform_humantime(args.until),
        args.description,
        args.note,
        args.is_downtime,
        &mirror,
        &mut state.events,
    )?;
    sync_maintenance_manifest_to_mirror_directory(
        &mirror,
        &state.events,
        &state.base_directory,
        &state.protocol_handlers,
        &state.format_handlers,
    )?;
    Ok(())
}

#[derive(Debug, Args)]
pub struct EditEventArgs {
    mirror: String,
    description: String,
    #[arg(short, long, env)]
    editor: String,
    #[arg(short, long, env, default_value = "toml")]
    format: String,
}

pub fn edit_event(args: EditEventArgs, mut state: ProgramState) -> Result<()> {
    let mirror = fetch_or_create_mirror_from_registry(args.mirror, &mut state.mirrors)?;
    let mut event =
        extract_maintenance_event_for_mirror(args.description, &mirror, &mut state.events)?;
    event = edit_record(
        args.editor,
        event,
        args.format,
        &state.protocol_handlers,
        &state.format_handlers,
    )?;
    register_maintenance_event_for_mirror(event, &mirror, &mut state.events)?;
    push_maintenance_manifest_to_mirror_directory(
        &mirror,
        &state.events,
        &state.base_directory,
        &state.protocol_handlers,
        &state.format_handlers,
    )?;
    Ok(())
}

#[derive(Debug, Args)]
pub struct UpdateEventArgs {
    mirror: String,
    description: String,
    #[arg(short, long)]
    start_time: Option<Timestamp>,
    #[arg(short, long)]
    end_time: Option<Timestamp>,
    #[arg(long)]
    new_description: Option<String>,
    #[arg(short, long)]
    is_downtime: Option<bool>,
    #[arg(short, long)]
    note: Option<Option<String>>,
}

pub fn update_event(args: UpdateEventArgs, mut state: ProgramState) -> Result<()> {
    let mirror = fetch_or_create_mirror_from_registry(args.mirror, &mut state.mirrors)?;
    edit_maintenance_event_for_mirror(
        args.description,
        args.new_description,
        &mirror,
        match args.start_time {
            Some(start_time) => Some(transform_humantime(start_time)),
            None => None,
        },
        match args.end_time {
            Some(end_time) => Some(transform_humantime(end_time)),
            None => None,
        },
        args.note,
        args.is_downtime,
        &mut state.events,
    )?;
    push_maintenance_manifest_to_mirror_directory(
        &mirror,
        &state.events,
        &state.base_directory,
        &state.protocol_handlers,
        &state.format_handlers,
    )?;
    Ok(())
}

#[derive(Debug, Args)]
pub struct DeleteEventArgs {
    mirror: String,
    description: String,
}

pub fn delete_event(args: DeleteEventArgs, mut state: ProgramState) -> Result<()> {
    let mirror = fetch_or_create_mirror_from_registry(args.mirror, &mut state.mirrors)?;
    delete_maintenance_event_for_mirror(args.description, &mirror, &mut state.events)?;
    Ok(())
}
