use super::entities::MaintenanceEvent;
use chrono::{DateTime, Utc};
use std::collections::{HashMap, HashSet};
use url::Url;

#[derive(Default, Clone, PartialEq, Debug)]
pub struct MaintenanceEventRegistry {
    events_by_mirror_name: HashMap<String, HashMap<String, MaintenanceEvent>>,
    events_by_url: HashMap<Url, HashMap<String, MaintenanceEvent>>,
    urls_by_mirror_name: HashMap<String, HashSet<Url>>,
    mirror_names_by_url: HashMap<Url, HashSet<String>>,
}

impl MaintenanceEventRegistry {
    pub fn get_event_by_mirror_name(
        &mut self,
        mirror_name: &String,
        description: &String,
    ) -> Option<&MaintenanceEvent> {
        let Some(events) = self.events_by_mirror_name.get(mirror_name) else {
            return None;
        };
        let event = events.get(description);
        event
    }

    pub fn extract_event_by_mirror_name(
        &mut self,
        mirror_name: &String,
        description: &String,
    ) -> Option<MaintenanceEvent> {
        let Some(events) = self.events_by_mirror_name.get_mut(mirror_name) else {
            return None;
        };
        let event = events.remove(description);
        event
    }

    pub fn extract_event_by_url(
        &mut self,
        url: &Url,
        description: &String,
    ) -> Option<MaintenanceEvent> {
        let Some(events) = self.events_by_url.get_mut(url) else {
            return None;
        };
        let event = events.remove(description);
        event
    }

    pub fn register_event_with_url(&mut self, url: &Url, event: MaintenanceEvent) {
        let events = self.events_by_url.entry(url.clone()).or_default();
        events.insert(event.description.clone(), event);
    }

    pub fn register_event_with_mirror_name(
        &mut self,
        mirror_name: &String,
        event: MaintenanceEvent,
    ) {
        let events = self
            .events_by_mirror_name
            .entry(mirror_name.clone())
            .or_default();
        events.insert(event.description.clone(), event);
    }

    pub fn get_urls_for_mirror_name(&self, mirror_name: &String) -> HashSet<Url> {
        let urls: HashSet<Url> = match self.urls_by_mirror_name.get(mirror_name) {
            None => HashSet::default(),
            Some(urls) => urls.clone(),
        };
        urls
    }

    pub fn has_mirror_announced_downtime(
        &self,
        mirror_name: &String,
        timestamp: DateTime<Utc>,
    ) -> bool {
        let Some(events) = self.events_by_mirror_name.get(mirror_name) else {
            return false;
        };
        for (_, event) in events.iter() {
            if timestamp >= event.starts_at && timestamp <= event.ends_at && event.is_downtime {
                return true;
            }
        }
        false
    }

    pub fn contains_event(&self, event: &MaintenanceEvent) -> bool {
        for event_hash_map in self.events_by_mirror_name.values() {
            let events: HashSet<&MaintenanceEvent> = event_hash_map.values().collect();
            if events.contains(event) {
                return true;
            }
        }
        for event_hash_map in self.events_by_url.values() {
            let events: HashSet<&MaintenanceEvent> = event_hash_map.values().collect();
            if events.contains(event) {
                return true;
            }
        }
        false
    }

    pub fn register_mirror_name_for_url(&mut self, mirror_name: &String, url: &Url) {
        let names = self.mirror_names_by_url.entry(url.clone()).or_default();
        names.insert(mirror_name.clone());
        let urls = self
            .urls_by_mirror_name
            .entry(mirror_name.clone())
            .or_default();
        urls.insert(url.clone());
    }

    pub fn get_events_by_mirror_name(&self, mirror_name: &String) -> HashSet<MaintenanceEvent> {
        let mut events: HashSet<MaintenanceEvent> = HashSet::default();

        match self.events_by_mirror_name.get(mirror_name) {
            Some(known_events) => {
                for event in known_events.values() {
                    events.insert(event.clone());
                }
            }
            None => {}
        };

        match self.urls_by_mirror_name.get(mirror_name) {
            None => {}
            Some(urls) => {
                for url in urls {
                    match self.events_by_url.get(url) {
                        None => {}
                        Some(known_events) => {
                            for event in known_events.values() {
                                events.insert(event.clone());
                            }
                        }
                    }
                }
            }
        }
        events
    }

    pub fn get_events_by_url(
        &self,
        url: &Url,
    ) -> HashMap<Option<String>, HashSet<MaintenanceEvent>> {
        let mut events_by_mirror_name: HashMap<Option<String>, HashSet<MaintenanceEvent>> =
            HashMap::default();
        match self.events_by_url.get(url) {
            Some(known_events) => {
                let mut events = HashSet::default();
                for event in known_events.values() {
                    events.insert(event.clone());
                }
                events_by_mirror_name.insert(None, events);
            }
            None => {}
        };

        match self.mirror_names_by_url.get(url) {
            None => {}
            Some(names) => {
                for name in names {
                    match self.events_by_mirror_name.get(name) {
                        None => {}
                        Some(known_events) => {
                            let mut events = HashSet::default();
                            for event in known_events.values() {
                                events.insert(event.clone());
                            }
                            events_by_mirror_name.insert(Some(name.clone()), events);
                        }
                    }
                }
            }
        }
        events_by_mirror_name
    }
    pub fn get_registered_mirror_names(&self) -> HashSet<String> {
        let mut set: HashSet<String> = HashSet::default();
        for mirror_name in self.events_by_mirror_name.keys() {
            set.insert(mirror_name.clone());
        }
        for mirror_name in self.urls_by_mirror_name.keys() {
            set.insert(mirror_name.clone());
        }
        set
    }

    pub fn get_registered_urls(&self) -> HashSet<Url> {
        let mut set: HashSet<Url> = HashSet::default();
        for url in self.events_by_url.keys() {
            set.insert(url.clone());
        }
        for url in self.mirror_names_by_url.keys() {
            set.insert(url.clone());
        }
        set
    }
}
