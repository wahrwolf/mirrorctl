use super::*;
use crate::url_handler::try_build_url_from_path_buf;
use crate::url_handler::{
    build_record_from_url, push_record_to_url, FormatHandlerRegistry, ProtocolHandlerRegistry,
};
use log::info;
use tempfile::TempDir;

#[test]
fn events_can_be_discovered_from_directory_with_directory() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let target_file = tmp_dir.path().join("maintenance.toml");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let good_record = build_test_manifest_with_events_without_mirror_names();
    let mirror_name = get_test_mirror_name();
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not store manifest");

    let mut registry = MaintenanceEventRegistry::default();

    let good_event = build_registered_event_with_downtime();
    assert!(!registry.contains_event(&good_event));

    let path = tmp_dir.path().to_path_buf();
    discover_maintenance_events_from_directory(
        &path,
        Some(&mirror_name),
        &mut registry,
        &protocol_handlers,
        &format_handlers,
    )
    .expect("Could not discover manifests");

    let bad_event = build_unregistered_event_with_downtime();
    assert!(registry
        .get_events_by_mirror_name(&mirror_name)
        .contains(&good_event));
    assert!(registry.contains_event(&good_event));
    assert!(!registry.contains_event(&bad_event));
}

#[test]
fn events_can_be_discovered_from_directory_without_directory() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let target_file = tmp_dir.path().join("maintenance.toml");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let good_record = build_test_manifest_with_events_without_mirror_names();
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not store manifest");

    let mut registry = MaintenanceEventRegistry::default();

    let good_event = build_registered_event_with_downtime();
    assert!(!registry.contains_event(&good_event));

    let path = tmp_dir.path().to_path_buf();
    discover_maintenance_events_from_directory(
        &path,
        None,
        &mut registry,
        &protocol_handlers,
        &format_handlers,
    )
    .expect("Could not discover manifests");

    let bad_event = build_unregistered_event_with_downtime();
    let mirror = build_test_mirror();
    let empty_set = HashSet::default();
    assert!(registry.contains_event(&good_event));
    assert!(!registry.contains_event(&bad_event));
    assert_eq!(empty_set, registry.get_events_by_mirror_name(&mirror.name));
}

#[test]
fn manifests_can_be_pushed_to_url() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let target_file = tmp_dir.path().join("maintenance.toml");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let mirror = build_test_mirror();
    let mut registry = MaintenanceEventRegistry::default();
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let good_events = get_registered_events_as_set();
    for event in good_events.iter() {
        registry.register_event_with_mirror_name(&mirror.name, event.clone());
    }
    push_maintenance_manifest_for_mirror_to_url(
        &mirror,
        &url,
        &registry,
        &protocol_handlers,
        &format_handlers,
    )
    .expect("Could not push url");

    let candidate: MaintenanceManifest =
        build_record_from_url(&url, &protocol_handlers, &format_handlers)
            .expect("Could not fetch manifest!");
    for event in good_events.iter() {
        assert!(candidate.contains_event(&event));
    }

    let bad_event = build_unregistered_event_with_downtime();
    assert!(!candidate.contains_event(&bad_event));
}

#[test]
fn manifests_can_be_fetched_from_url() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let target_file = tmp_dir.path().join("maintenance.toml");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let good_record = build_test_manifest_with_events_without_mirror_names();

    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let mut registry = MaintenanceEventRegistry::default();

    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not store manifest");
    fetch_maintenance_manifest_from_url(&url, &mut registry, &protocol_handlers, &format_handlers)
        .expect("Could not fetch manifest!");

    let good_events = get_registered_events_as_set();
    for event in good_events {
        assert!(registry.contains_event(&event));
    }
    let bad_event = build_unregistered_event_with_downtime();
    assert!(!registry.contains_event(&bad_event));
}

#[test]
fn manifest_can_be_fetched_for_single_mirror() {
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let target_file = tmp_dir.path().join("maintenance.toml");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");

    let good_record = build_test_manifest_with_events_without_mirror_names();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not store manifest");

    let mut mirror = build_test_mirror();
    mirror.register_maintenance_url(url.clone());

    let mut registry = MaintenanceEventRegistry::default();
    fetch_maintenance_manifest_for_mirror_from_url(
        &mirror.name,
        &url,
        &mut registry,
        &protocol_handlers,
        &format_handlers,
    )
    .expect("Could not fetch manifest!");

    let good_events = get_registered_events_as_set();
    for event in good_events {
        assert!(registry.contains_event(&event));
    }
    let bad_event = build_unregistered_event_with_downtime();
    assert!(!registry.contains_event(&bad_event));
}

#[test]
fn events_can_be_synced_for_single_mirror() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let source_file = tmp_dir.path().join("source.toml");
    let source_url = try_build_url_from_path_buf(&source_file).expect("Could not build url");
    let target_file = tmp_dir.path().join("target.toml");
    let target_url = try_build_url_from_path_buf(&target_file).expect("Could not build url");

    let source_manifest = build_test_manifest_with_events_with_mirror_names();
    info!("manifest with some events:\n{:?}\n\n", source_manifest);
    push_record_to_url(
        &source_url,
        &source_manifest,
        &protocol_handlers,
        &format_handlers,
    )
    .expect("Could not store manifest");

    let target_manifest = build_test_manifest_without_events();
    info!("manifest without events:\n{:?}\n\n", target_manifest);
    push_record_to_url(
        &target_url,
        &target_manifest,
        &protocol_handlers,
        &format_handlers,
    )
    .expect("Could not store manifest");

    let mut mirror = build_test_mirror();
    info!("mirror without maintenance urls:\n{:?}\n\n", mirror);
    register_maintenance_url_for_mirror(&mut mirror, &source_url)
        .expect("Could not register source url");
    register_maintenance_url_for_mirror(&mut mirror, &target_url)
        .expect("Could not register target url");
    info!("mirror without maintenance urls:\n{:?}\n\n", mirror);

    sync_maintenance_manifests_for_mirror(&mirror, &protocol_handlers, &format_handlers)
        .expect("Could not sync manifests");

    let candidate: MaintenanceManifest =
        build_record_from_url(&target_url, &protocol_handlers, &format_handlers)
            .expect("Could not fetch manifest!");
    let good_events = get_registered_events_as_set();
    for event in good_events {
        assert!(candidate.contains_event(&event));
    }
    let bad_event = build_unregistered_event_with_downtime();
    assert!(!candidate.contains_event(&bad_event));
}

#[test]
fn urls_can_be_registered_for_mirror() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let good_file = tmp_dir.path().join("good.toml");
    let good_url = try_build_url_from_path_buf(&good_file).expect("Could not build url");
    let bad_file = tmp_dir.path().join("bad.toml");
    let bad_url = try_build_url_from_path_buf(&bad_file).expect("Could not build url");

    let mut mirror = build_test_mirror();
    register_maintenance_url_for_mirror(&mut mirror, &good_url).expect("Could not register url!");

    let candidate = mirror.get_maintenance_urls();
    assert!(candidate.contains(&good_url));
    assert!(!candidate.contains(&bad_url));
}
