use super::*;
use log::info;

#[test]
fn events_can_be_registered_into_registry_with_url() {
    let mut candidate = build_registry_without_events();
    let url = get_url_for_test_mirror();
    let good_events = get_registered_events_as_set();

    for event in good_events {
        candidate.register_event_with_url(&url, event.clone());
        assert!(candidate.contains_event(&event));
    }

    let bad_event = build_unregistered_event_with_downtime();
    assert!(!candidate.contains_event(&bad_event));
}

#[test]
fn events_can_be_registered_into_registry_with_mirror_name() {
    let mut candidate = build_registry_without_events();
    let name = get_test_mirror_name();
    let good_events = get_registered_events_as_set();

    for event in good_events {
        candidate.register_event_with_mirror_name(&name, event.clone());
        assert!(candidate.contains_event(&event));
    }

    let bad_event = build_unregistered_event_with_downtime();
    assert!(!candidate.contains_event(&bad_event));
}

#[test]
fn downtime_can_be_looked_up() {
    let candidate = build_registry_with_event_with_mirror_names_without_url();
    let name = get_test_mirror_name();
    let good_time = get_datetime_for_registered_downtime_event();

    assert!(candidate.has_mirror_announced_downtime(&name, good_time));

    let bad_times = get_uptime_events();
    for time in bad_times {
        assert!(!candidate.has_mirror_announced_downtime(&name, time));
    }

    let bad_time = get_datetime_for_unregistered_downtime_event();
    assert!(!candidate.has_mirror_announced_downtime(&name, bad_time));
}

#[test]
fn events_can_be_fetched_with_url() {
    let registry = build_registry_with_event_without_mirror_names_with_url();
    info!("{:?}", registry);
    let url = get_url_for_test_mirror();
    info!("{:?}", url);

    let good_events = get_registered_events_as_set();
    info!("{:?}", good_events);
    for event in good_events.iter() {
        assert!(registry.contains_event(&event));
    }

    let candidate = registry.get_events_by_url(&url);
    info!("{:?}", candidate);
    assert_eq!(Some(&good_events), candidate.get(&None));

    let name = get_test_mirror_name();
    info!("{:?}", name);
    let empty_set = HashSet::new();
    assert_eq!(empty_set, registry.get_events_by_mirror_name(&name));
}

#[test]
fn events_can_be_fetched_with_mirror_name() {
    let registry = build_registry_with_event_with_mirror_names_without_url();
    info!("{:?}", registry);
    let name = get_test_mirror_name();
    info!("{:?}", name);

    let good_events = get_registered_events_as_set();
    info!("{:?}", good_events);
    for event in good_events.iter() {
        assert!(registry.contains_event(&event));
    }

    let candidate = registry.get_events_by_mirror_name(&name);
    info!("{:?}", candidate);
    assert_eq!(good_events, candidate);

    let url = get_url_for_test_mirror();
    info!("{:?}", url);
    let empty_map = HashMap::new();
    assert_eq!(empty_map, registry.get_events_by_url(&url));
}

#[test]
fn mirrors_can_be_registered_for_url() {
    let mut registry = build_registry_with_event_with_mirror_names_without_url();
    let name = get_test_mirror_name();
    let good_events = get_registered_events_as_set();

    let candidate = registry.get_events_by_mirror_name(&name);
    info!("Candidate for set with some events:\n{:?}\n\n", candidate);
    assert_eq!(good_events, candidate);

    let url = get_url_for_test_mirror();
    let empty_map = HashMap::new();
    let candidate = registry.get_events_by_url(&url);
    info!("Candidate for empty map:\n{:?}\n\n", candidate);
    assert_eq!(empty_map, candidate);

    registry.register_mirror_name_for_url(&name, &url);

    let full_map: HashMap<Option<String>, HashSet<MaintenanceEvent>> =
        get_registered_events_as_map_with_mirror_name();
    info!("Map with some events:\n{:?}\n\n", full_map);
    let candidate = registry.get_events_by_url(&url);
    info!("Candidate for map with some events:\n{:?}\n\n", candidate);
    assert_eq!(full_map, candidate);
}
