use super::*;
use crate::url_handler::try_build_url_from_path_buf;
use crate::url_handler::{
    build_record_from_url, push_record_to_url, FormatHandlerRegistry, ProtocolHandlerRegistry,
};
use tempfile::TempDir;

#[test]
fn maintenance_event_can_be_stored_in_toml() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let target_file = tmp_dir.path().join("maintenance_event.toml");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let good_record = build_unregistered_event_with_downtime();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: MaintenanceEvent =
        build_record_from_url(&url, &protocol_handlers, &format_handlers)
            .expect("Could not create struct!");
    assert_eq!(good_record, candidate.clone());

    let bad_record = build_registered_event_without_downtime();
    assert_ne!(bad_record, candidate);
}

#[test]
fn maintenance_event_can_be_stored_in_json() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let target_file = tmp_dir.path().join("maintenance_event.json");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let good_record = build_unregistered_event_with_downtime();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: MaintenanceEvent =
        build_record_from_url(&url, &protocol_handlers, &format_handlers)
            .expect("Could not create struct!");
    assert_eq!(good_record, candidate.clone());

    let bad_record = build_registered_event_without_downtime();
    assert_ne!(bad_record, candidate);
}

#[test]
fn maintenance_events_can_be_compared() {
    let good_event = build_unregistered_event_with_downtime();
    let bad_event = build_unregistered_event_without_downtime();

    assert_eq!(&good_event, &good_event);
    assert_eq!(&good_event, &good_event.clone());
    assert_ne!(&bad_event, &good_event);
}
