use super::*;
use crate::url_handler::try_build_url_from_path_buf;
use crate::url_handler::{
    build_record_from_url, push_record_to_url, FormatHandlerRegistry, ProtocolHandlerRegistry,
};
use tempfile::TempDir;

#[test]
fn maintenance_manifest_without_events_can_be_stored_in_toml() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let target_file = tmp_dir.path().join("maintenance_manifest.toml");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let good_record = build_test_manifest_without_events();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: MaintenanceManifest =
        build_record_from_url(&url, &protocol_handlers, &format_handlers)
            .expect("Could not create struct!");
    assert_eq!(good_record, candidate.clone());

    let bad_record = build_test_manifest_with_events_without_mirror_names();
    assert_ne!(bad_record, candidate);
}

#[test]
fn maintenance_manifest_without_mirror_names_can_be_stored_in_toml() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let target_file = tmp_dir.path().join("maintenance_manifest.toml");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let good_record = build_test_manifest_with_events_without_mirror_names();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: MaintenanceManifest =
        build_record_from_url(&url, &protocol_handlers, &format_handlers)
            .expect("Could not create struct!");
    assert_eq!(good_record, candidate.clone());

    let bad_record = build_test_manifest_without_events();
    assert_ne!(bad_record, candidate);
}

#[test]
fn maintenance_manifest_with_mirror_names_can_be_stored_in_toml() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let target_file = tmp_dir.path().join("maintenance_manifest.toml");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let good_record = build_test_manifest_with_events_with_mirror_names();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: MaintenanceManifest =
        build_record_from_url(&url, &protocol_handlers, &format_handlers)
            .expect("Could not create struct!");
    assert_eq!(good_record, candidate.clone());

    let bad_record = build_test_manifest_without_events();
    assert_ne!(bad_record, candidate);
}

#[test]
fn maintenance_manifest_without_events_can_be_stored_in_json() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let target_file = tmp_dir.path().join("maintenance_manifest.json");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let good_record = build_test_manifest_without_events();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: MaintenanceManifest =
        build_record_from_url(&url, &protocol_handlers, &format_handlers)
            .expect("Could not create struct!");
    assert_eq!(good_record, candidate.clone());

    let bad_record = build_test_manifest_with_events_without_mirror_names();
    assert_ne!(bad_record, candidate);
}

#[test]
fn maintenance_manifest_with_mirror_names_can_be_stored_in_json() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let target_file = tmp_dir.path().join("maintenance_manifest.json");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let good_record = build_test_manifest_with_events_with_mirror_names();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: MaintenanceManifest =
        build_record_from_url(&url, &protocol_handlers, &format_handlers)
            .expect("Could not create struct!");
    assert_eq!(good_record, candidate.clone());

    let bad_record = build_test_manifest_without_events();
    assert_ne!(bad_record, candidate);
}

#[test]
fn maintenance_manifest_without_mirror_names_can_be_stored_in_json() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let target_file = tmp_dir.path().join("maintenance_manifest.json");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let good_record = build_test_manifest_with_events_without_mirror_names();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: MaintenanceManifest =
        build_record_from_url(&url, &protocol_handlers, &format_handlers)
            .expect("Could not create struct!");
    assert_eq!(good_record, candidate.clone());

    let bad_record = build_test_manifest_without_events();
    assert_ne!(bad_record, candidate);
}
