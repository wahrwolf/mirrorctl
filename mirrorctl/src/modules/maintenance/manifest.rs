use super::entities::MaintenanceEvent;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::collections::{hash_map::IntoIter as HashMapIter, HashMap, HashSet};

#[derive(Default, Clone, Eq, PartialEq, Debug, Serialize, Deserialize)]
pub struct MaintenanceManifest {
    pub contact_email: Option<String>,
    pub last_updated: DateTime<Utc>,
    events_by_mirror_name: HashMap<String, HashSet<MaintenanceEvent>>,
    events: HashSet<MaintenanceEvent>,
}

impl MaintenanceManifest {
    pub fn new() -> MaintenanceManifest {
        MaintenanceManifest {
            contact_email: None,
            events_by_mirror_name: HashMap::default(),
            events: HashSet::default(),
            last_updated: Utc::now(),
        }
    }
}

impl MaintenanceManifest {
    fn get_events_or_default_by_mirror_name(
        &mut self,
        mirror_name: Option<String>,
    ) -> &mut HashSet<MaintenanceEvent> {
        let events = match mirror_name {
            Some(name) => self.events_by_mirror_name.entry(name).or_default(),
            None => &mut self.events,
        };
        events
    }

    pub fn insert_event(&mut self, mirror_name: Option<String>, event: MaintenanceEvent) {
        if !self.events.contains(&event) {
            let events = self.get_events_or_default_by_mirror_name(mirror_name);
            events.insert(event);
            self.last_updated = Utc::now();
        }
    }

    pub fn insert_events(
        &mut self,
        mirror_name: Option<String>,
        events: HashSet<MaintenanceEvent>,
    ) {
        let mut has_changed = false;
        for event in events.iter() {
            if !self.events.contains(event) {
                let registered_events =
                    self.get_events_or_default_by_mirror_name(mirror_name.clone());
                registered_events.insert(event.clone());
                has_changed = true;
            }
        }
        if has_changed {
            self.last_updated = Utc::now();
        }
    }

    pub fn from_hash_map(map: HashMap<String, HashSet<MaintenanceEvent>>) -> MaintenanceManifest {
        let mut manifest = MaintenanceManifest::new();
        for (name, events) in map.clone().drain() {
            manifest.insert_events(Some(name), events);
        }
        manifest
    }

    pub fn from_hash_set(
        mirror_name: Option<String>,
        set: HashSet<MaintenanceEvent>,
    ) -> MaintenanceManifest {
        let mut manifest = MaintenanceManifest::new();
        manifest.insert_events(mirror_name, set);
        manifest
    }

    pub fn contains_event(&self, event: &MaintenanceEvent) -> bool {
        for (_, events) in self.events_by_mirror_name.iter() {
            if events.contains(event) {
                return true;
            }
        }
        false
    }
}

impl IntoIterator for MaintenanceManifest {
    type Item = (Option<String>, HashSet<MaintenanceEvent>);
    type IntoIter = HashMapIter<Option<String>, HashSet<MaintenanceEvent>>;

    fn into_iter(self) -> Self::IntoIter {
        let mut map: HashMap<Option<String>, HashSet<MaintenanceEvent>> = HashMap::default();
        for (name, events) in self.events_by_mirror_name {
            map.insert(Some(name), events);
        }
        map.insert(None, self.events);
        map.into_iter()
    }
}

impl FromIterator<MaintenanceEvent> for MaintenanceManifest {
    fn from_iter<I: IntoIterator<Item = MaintenanceEvent>>(iter: I) -> Self {
        let mut manifest = MaintenanceManifest::new();
        for event in iter {
            manifest.insert_event(None, event.clone());
        }
        manifest
    }
}
