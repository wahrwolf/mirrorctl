use super::entities::MaintenanceEvent;
use super::manifest::MaintenanceManifest;
use super::registry::MaintenanceEventRegistry;
use crate::cli::get_mirror_directory_by_mirror;
use crate::modules::mirror::Mirror;
use crate::url_handler::{
    build_record_from_url, push_record_to_url, try_build_url_from_path_buf, FormatHandlerRegistry,
    ProtocolHandlerRegistry,
};
use anyhow::{Context, Result};
use chrono::{DateTime, Utc};
use log::{debug, info};
use std::collections::HashMap;
use std::fs::{read_dir, DirEntry};
use std::path::PathBuf;
use url::Url;

pub fn extract_maintenance_event_for_mirror(
    description: String,
    mirror: &Mirror,
    registry: &mut MaintenanceEventRegistry,
) -> Result<MaintenanceEvent> {
    let event = registry
        .extract_event_by_mirror_name(&mirror.name, &description)
        .expect("Could not find event!");
    Ok(event)
}

pub fn register_maintenance_event_for_mirror(
    event: MaintenanceEvent,
    mirror: &Mirror,
    registry: &mut MaintenanceEventRegistry,
) -> Result<()> {
    registry.register_event_with_mirror_name(&mirror.name, event);
    Ok(())
}

pub fn create_maintenance_event_for_mirror(
    start_time: DateTime<Utc>,
    end_time: DateTime<Utc>,
    description: String,
    note: Option<String>,
    is_downtime: bool,
    mirror: &Mirror,
    registry: &mut MaintenanceEventRegistry,
) -> Result<()> {
    let event =
        MaintenanceEvent::new_with_args(description, note, is_downtime, start_time, end_time);
    registry.register_event_with_mirror_name(&mirror.name, event);
    Ok(())
}

pub fn create_maintenance_event_for_url(
    start_time: DateTime<Utc>,
    end_time: DateTime<Utc>,
    description: String,
    note: Option<String>,
    is_downtime: bool,
    url: &Url,
    registry: &mut MaintenanceEventRegistry,
) -> Result<()> {
    let event =
        MaintenanceEvent::new_with_args(description, note, is_downtime, start_time, end_time);
    registry.register_event_with_url(&url, event);
    Ok(())
}

pub fn delete_maintenance_event_for_mirror(
    description: String,
    mirror: &Mirror,
    registry: &mut MaintenanceEventRegistry,
) -> Result<()> {
    let _ = registry
        .extract_event_by_mirror_name(&mirror.name, &description)
        .expect("Could not find event!");
    Ok(())
}

pub fn edit_maintenance_event_for_mirror(
    current_description: String,
    new_description: Option<String>,
    mirror: &Mirror,
    start_time: Option<DateTime<Utc>>,
    end_time: Option<DateTime<Utc>>,
    note: Option<Option<String>>,
    is_downtime: Option<bool>,
    registry: &mut MaintenanceEventRegistry,
) -> Result<()> {
    let mut event = registry
        .extract_event_by_mirror_name(&mirror.name, &current_description)
        .expect("Could not find event!");
    match start_time {
        Some(start_time) => event.starts_at = start_time,
        None => {}
    }
    match end_time {
        Some(end_time) => event.ends_at = end_time,
        None => {}
    }
    match end_time {
        Some(end_time) => event.ends_at = end_time,
        None => {}
    }
    match note {
        Some(note) => event.note = note,
        None => {}
    }
    match is_downtime {
        Some(is_downtime) => event.is_downtime = is_downtime,
        None => {}
    }
    match new_description {
        Some(new_description) => event.description = new_description,
        None => {}
    }
    registry.register_event_with_mirror_name(&mirror.name, event);
    Ok(())
}

pub fn get_default_maintenance_manifest_url(
    mirror: &Mirror,
    base_directory: &PathBuf,
) -> Result<Url> {
    let default_url = try_build_url_from_path_buf(
        &get_mirror_directory_by_mirror(base_directory, mirror).join("maintenance.toml"),
    )?;
    Ok(default_url)
}

pub fn push_maintenance_manifest_to_mirror_directory(
    mirror: &Mirror,
    registry: &MaintenanceEventRegistry,
    base_directory: &PathBuf,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    let default_url = get_default_maintenance_manifest_url(mirror, base_directory)?;
    push_maintenance_manifest_for_mirror_to_url(
        mirror,
        &default_url,
        registry,
        protocol_handlers,
        format_handlers,
    )
}

pub fn sync_maintenance_manifest_for_mirror_to_url(
    mirror: &Mirror,
    url: &Url,
    registry: &MaintenanceEventRegistry,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    let mut manifest: MaintenanceManifest =
        match build_record_from_url(&url, protocol_handlers, format_handlers) {
            Ok(manifest) => manifest,
            Err(error) => {
                info!("Could not fetch record due to {}", error);
                MaintenanceManifest::new()
            }
        };
    let events = registry.get_events_by_mirror_name(&mirror.name);
    manifest.insert_events(Some(mirror.name.clone()), events.clone());
    push_record_to_url(&url, &manifest, protocol_handlers, format_handlers)?;
    Ok(())
}

pub fn sync_maintenance_manifest_to_mirror_directory(
    mirror: &Mirror,
    registry: &MaintenanceEventRegistry,
    base_directory: &PathBuf,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    let default_url = try_build_url_from_path_buf(
        &get_mirror_directory_by_mirror(base_directory, mirror).join("maintenance.toml"),
    )?;
    sync_maintenance_manifest_for_mirror_to_url(
        &mirror,
        &default_url,
        &registry,
        protocol_handlers,
        format_handlers,
    )?;
    Ok(())
}

pub fn push_maintenance_event_for_mirror_to_url(
    mirror: &Mirror,
    description: &String,
    url: &Url,
    registry: &mut MaintenanceEventRegistry,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    let event: &MaintenanceEvent = registry
        .get_event_by_mirror_name(&mirror.name, &description)
        .expect("Could not find event!");
    push_record_to_url(url, event, protocol_handlers, format_handlers)
}

pub fn fetch_maintenance_event_for_mirror_from_url(
    mirror: &Mirror,
    url: &Url,
    registry: &mut MaintenanceEventRegistry,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    let event: MaintenanceEvent = build_record_from_url(url, protocol_handlers, format_handlers)?;
    registry.register_event_with_mirror_name(&mirror.name, event);
    Ok(())
}

pub fn push_maintenance_manifest_for_mirror_to_url(
    mirror: &Mirror,
    url: &Url,
    registry: &MaintenanceEventRegistry,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    let manifest = MaintenanceManifest::from_hash_set(
        Some(mirror.name.clone()),
        registry.get_events_by_mirror_name(&mirror.name),
    );
    push_record_to_url(url, &manifest, protocol_handlers, format_handlers)
}

pub fn register_events_from_manifest(
    url: &Url,
    manifest: &MaintenanceManifest,
    registry: &mut MaintenanceEventRegistry,
) -> Result<()> {
    for (mirror_name, events) in manifest.clone() {
        match mirror_name {
            None => {
                for event in events {
                    registry.register_event_with_url(&url, event);
                }
            }
            Some(name) => {
                for event in events {
                    registry.register_event_with_mirror_name(&name, event);
                }
                registry.register_mirror_name_for_url(&name, &url);
            }
        }
    }
    Ok(())
}

pub fn fetch_maintenance_manifest_from_url(
    url: &Url,
    registry: &mut MaintenanceEventRegistry,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    let manifest: MaintenanceManifest =
        build_record_from_url(url, protocol_handlers, format_handlers)?;
    register_events_from_manifest(url, &manifest, registry)?;
    Ok(())
}

pub fn discover_maintenance_events_from_directory(
    directory: &PathBuf,
    mirror_name: Option<&String>,
    registry: &mut MaintenanceEventRegistry,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    fn try_to_add_file_to_registry(
        dir_entry: &DirEntry,
        mirror_name: Option<&String>,
        registry: &mut MaintenanceEventRegistry,
        protocol_handlers: &ProtocolHandlerRegistry,
        format_handlers: &FormatHandlerRegistry,
    ) -> Result<()> {
        let path = dir_entry.path();
        debug!("Trying to add {:?}", path);

        let file_stem = path
            .file_stem()
            .context("Can not fetch file stem from path!")?
            .to_str();

        let url = try_build_url_from_path_buf(&path).context("Could not build url")?;
        if Some("maintenance") != file_stem {
            anyhow::bail!(
                "File stem {:?} found instead of {:?}!",
                file_stem,
                Some("maintenance")
            );
        } else {
            debug!("File matches the default filter");
        };
        match mirror_name {
            Some(mirror_name) => {
                debug!("Adding manifest for {}", mirror_name);
                fetch_maintenance_manifest_for_mirror_from_url(
                    mirror_name,
                    &url,
                    registry,
                    protocol_handlers,
                    format_handlers,
                )?
            }
            None => {
                debug!("Adding manifest without a mirror");
                fetch_maintenance_manifest_from_url(
                    &url,
                    registry,
                    protocol_handlers,
                    format_handlers,
                )?
            }
        }
        Ok(())
    }

    info!(
        "Scanning {:?} for maintenance manifests{}",
        directory,
        match mirror_name {
            Some(name) => format!(" for {}", name),
            None => "".to_string(),
        }
    );
    for possible_manifest in read_dir(directory)? {
        let possible_manifest = possible_manifest?;
        match try_to_add_file_to_registry(
            &possible_manifest,
            mirror_name,
            registry,
            protocol_handlers,
            format_handlers,
        ) {
            Ok(_) => continue,
            Err(error) => {
                debug!("Could not add file due to {:?}", error);
                continue;
            }
        };
    }
    Ok(())
}

pub fn fetch_maintenance_manifest_for_mirror_from_url(
    mirror_name: &String,
    url: &Url,
    registry: &mut MaintenanceEventRegistry,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    debug!("Registering {} for {}", mirror_name, url);
    fetch_maintenance_manifest_from_url(url, registry, protocol_handlers, format_handlers)?;
    registry.register_mirror_name_for_url(mirror_name, url);
    Ok(())
}

pub fn sync_maintenance_manifests_for_mirror(
    mirror: &Mirror,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    let mut manifests: HashMap<Url, MaintenanceManifest> = HashMap::default();
    let mut registry = MaintenanceEventRegistry::default();

    for url in mirror.get_maintenance_urls() {
        debug!("Fetching manifest from {}", url);
        let manifest: MaintenanceManifest =
            build_record_from_url(url, protocol_handlers, format_handlers)?;
        manifests.insert(url.clone(), manifest.clone());
        register_events_from_manifest(&url, &manifest, &mut registry)?;
    }

    let events = registry.get_events_by_mirror_name(&mirror.name);
    for (url, manifest) in &mut manifests {
        debug!("Pushing manifest to {}", url);
        manifest.insert_events(Some(mirror.name.clone()), events.clone());
        push_record_to_url(&url, manifest, protocol_handlers, format_handlers)?;
    }

    Ok(())
}

pub fn register_maintenance_url_for_mirror(mirror: &mut Mirror, url: &Url) -> Result<()> {
    mirror.register_maintenance_url(url.clone());
    Ok(())
}
