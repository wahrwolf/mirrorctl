use anyhow::Context;
use anyhow::Result;
use clap::Args;
use std::path::PathBuf;
use std::process::Command;

#[derive(Debug, Args)]
pub struct SignArgs {
    #[arg(short, long)]
    certificate: PathBuf,
    #[arg(short, long)]
    privkey: PathBuf,
}
pub fn sign(args: SignArgs) -> Result<()> {
    let private_key_path_as_string = args
        .privkey
        .to_str()
        .context("Could not parse private_key path to string")?;
    let certificate_path_as_string = args
        .certificate
        .to_str()
        .context("Could not parse target certificate path to string")?;

    let output = Command::new("openssl")
        .args(&[
            "cms",
            "-sign",
            "-binary",
            "-nocerts",
            "-noattr",
            "-outform",
            "DER",
            "-out",
            "test.cms.sig",
            "-in",
            "test.txt",
            "-signer",
            &certificate_path_as_string,
            "-inkey",
            &private_key_path_as_string,
        ])
        .output()
        .context("failed to execute process")?;

    if !output.status.success() {
        let result_text = String::from_utf8_lossy(&output.stderr);
        anyhow::bail!("Could not sign file due to {result_text}");
    }

    return Ok(());
}
