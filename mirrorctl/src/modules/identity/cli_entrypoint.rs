use anyhow::{anyhow, Context, Result};
use clap::Args;
use std::fs;
use std::path::PathBuf;
use std::process::Command;

use std::fs::OpenOptions;
use std::io::Write;

use tempfile::TempDir;

#[derive(Debug, Args)]
pub struct IdentityArgs {
    #[arg(short, long)]
    certificate: PathBuf,
    #[arg(short, long)]
    privkey: PathBuf,
    #[arg(short, long)]
    days: u64,
}
pub fn identity(args: IdentityArgs) -> Result<()> {
    if args.privkey.exists() {
        anyhow::bail!("--privkey already exists: {:?}", args.privkey);
    }
    if args.certificate.exists() {
        anyhow::bail!("--certificate already exists: {:?}", args.certificate);
    }

    let tmp_dir = TempDir::new()?;

    let codesigning_dir = tmp_dir.path().join(".codesigning/");
    let ca_dir = codesigning_dir.join("ca");

    let ca_conf = &ca_dir.join("certificate_authority.cnf");
    let ca_subj = "/C=DE/ST=Berlin/L=Berlin/O=Arch Linux/OU=Mirror Administrators/emailAddress=mirrors@archlinux.org/CN=Arch Linux Mirror Administrators (Ephemeral CA)";
    let ca_cert = ca_dir.join("cacert.pem");
    let ca_key = ca_dir.join("private").join("cakey.pem");

    let codesigning_conf = &codesigning_dir.join("code_signing.cnf");
    let codesigning_subj = "/C=DE/ST=Berlin/L=Berlin/O=Arch Linux/OU=Mirror Administrators/emailAddress=mirrors@archlinux.org/CN=Arch Linux Mirror Administrators (Ephemeral Signing Key)";

    for subdir in &["private", "newcerts", "crl"] {
        fs::create_dir_all(ca_dir.join(subdir))?;
    }

    for target in &[&codesigning_conf, &ca_conf] {
        fs::copy("/etc/ssl/openssl.cnf", target)?;
    }

    fs::write(ca_dir.join("index.txt"), "")?;
    fs::write(ca_dir.join("serial"), "1000")?;

    let lines: String = fs::read_to_string(ca_conf)?.parse()?;
    let fixed_lines = lines.replace(
        "/etc/ssl",
        ca_dir.to_str().context("Can not unwrap ca_dir name")?,
    );

    fs::write(ca_conf, fixed_lines)?;

    let output = Command::new("openssl")
        .args(&[
            "req",
            "-newkey",
            "ec",
            "-pkeyopt",
            "ec_paramgen_curve:P-384",
            "-nodes",
            "-x509",
            "-new",
            "-sha256",
            "-keyout",
            ca_key.to_str().context("Can not unwrap ca_key as string")?,
            "-config",
            ca_conf
                .to_str()
                .context("Can not unwrap ca_key as string")?,
            "-out",
            ca_cert
                .to_str()
                .context("Can not unwrap ca_key as string")?,
            "-subj",
            ca_subj,
            "-days",
            &args.days.to_string(),
        ])
        .output()
        .context("failed to execute process")?;

    if !output.status.success() {
        let result_text = String::from_utf8_lossy(&output.stderr);
        anyhow::bail!("Could not create ca key due to {result_text}");
    }

    let extension_text = "
[codesigning]
keyUsage=digitalSignature
extendedKeyUsage=codeSigning, clientAuth, emailProtection
";

    for target in &[&codesigning_conf, &ca_conf] {
        let mut file = OpenOptions::new()
            .append(true)
            .open(target)
            .with_context(|| anyhow!("Failed to open {target:?}"))?;
        writeln!(file, "{}", &extension_text)?;
    }

    let output = Command::new("openssl")
        .args(&[
            "req",
            "-newkey",
            "ec",
            "-pkeyopt",
            "ec_paramgen_curve:P-384",
            "-keyout",
            args.privkey
                .to_str()
                .context("Can not unwrap private key path")?,
            "-nodes",
            "-sha256",
            "-out",
            &(args
                .certificate
                .to_str()
                .context("Can no unwrap certificate path")?
                .to_owned()
                + ".csr"),
            "-config",
            codesigning_conf
                .to_str()
                .context("Can not unwrap codesigning conf path")?,
            "-subj",
            codesigning_subj,
            "-extensions",
            "codesigning",
        ])
        .output()
        .context("failed to execute process")?;

    if !output.status.success() {
        let result_text = String::from_utf8_lossy(&output.stderr);
        anyhow::bail!("Could not create private key due to {result_text}");
    }

    let output = Command::new("openssl")
        .args(&[
            "ca",
            "-batch",
            "-config",
            ca_conf
                .to_str()
                .context("Can not unwrap ca_key as string")?,
            "-extensions",
            "codesigning",
            "-days",
            &args.days.to_string(),
            "-notext",
            "-md",
            "sha256",
            "-in",
            &(args
                .certificate
                .to_str()
                .context("Can no unwrap certificate path")?
                .to_owned()
                + ".csr"),
            "-out",
            args.certificate
                .to_str()
                .context("Can no unwrap certificate path")?,
        ])
        .output()
        .context("failed to execute process")?;

    if !output.status.success() {
        let result_text = String::from_utf8_lossy(&output.stderr);
        anyhow::bail!("Could not create public key due to {result_text}");
    }

    return Ok(());
}
