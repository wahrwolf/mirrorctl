use super::entities::Mirror;
use super::manifest::MirrorManifest;
use super::registry::MirrorRegistry;
use crate::cli::{get_mirror_directory_by_mirror, get_mirror_directory_by_name};
use crate::url_handler::try_build_url_from_path_buf;
use crate::url_handler::{
    build_record_from_url, push_record_to_url, FormatHandlerRegistry, ProtocolHandlerRegistry,
};
use anyhow::Result;
use log::info;
use std::fs::{read_dir, DirEntry};
use std::path::{Path, PathBuf};
use url::Url;

pub fn add_mirrors_from_manifest(
    manifest: MirrorManifest,
    registry: &mut MirrorRegistry,
) -> Result<()> {
    for (_, mirror) in manifest {
        registry.add_mirror(mirror.clone());
    }
    Ok(())
}

pub fn add_mirrors_from_url(
    url: Url,
    registry: &mut MirrorRegistry,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    let manifest: MirrorManifest = build_record_from_url(&url, protocol_handlers, format_handlers)?;
    for (name, mirror) in manifest {
        registry.add_mirror(mirror);
        registry.register_url_for_mirror(&url, &&name);
    }
    Ok(())
}

pub fn generate_url_for_mirror_name(base_directory: &PathBuf, mirror_name: &String) -> Result<Url> {
    let directory = get_mirror_directory_by_name(base_directory, mirror_name);
    let target_file = directory.join("mirror.toml");
    let url = try_build_url_from_path_buf(&target_file)?;
    Ok(url)
}

pub fn generate_url_for_mirror(base_directory: &PathBuf, mirror: &Mirror) -> Result<Url> {
    let directory = get_mirror_directory_by_mirror(base_directory, mirror);
    let target_file = directory.join("mirror.toml");
    let url = try_build_url_from_path_buf(&target_file)?;

    Ok(url)
}

pub fn add_mirrors_from_directory(
    directory: &Path,
    registry: &mut MirrorRegistry,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    fn try_to_add_file_to_registry(
        dir_entry: &DirEntry,
        registry: &mut MirrorRegistry,
        protocol_handlers: &ProtocolHandlerRegistry,
        format_handlers: &FormatHandlerRegistry,
    ) -> Result<()> {
        let path = dir_entry.path();
        let url = try_build_url_from_path_buf(&path).expect("Could not build url");
        add_mirrors_from_url(url, registry, protocol_handlers, format_handlers)
    }

    for possible_manifest in read_dir(directory)? {
        let possible_manifest = possible_manifest?;
        let Ok(()) = try_to_add_file_to_registry(
            &possible_manifest,
            registry,
            protocol_handlers,
            format_handlers,
        ) else {
            continue;
        };
    }
    Ok(())
}

pub fn fetch_or_create_mirror_from_registry(
    mirror_name: String,
    registry: &mut MirrorRegistry,
) -> Result<&Mirror> {
    let new_mirror = Mirror::new(mirror_name.clone());
    let mirror = registry.get_or_insert(&mirror_name, new_mirror);
    Ok(mirror)
}

pub fn sync_mirror_manifest_for_mirror_to_url(
    mirror_name: &String,
    url: &Url,
    registry: &MirrorRegistry,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    let mirror = registry
        .get_mirror_by_name(mirror_name)
        .expect("Could not find mirror!");
    let mut manifest: MirrorManifest =
        match build_record_from_url(&url, protocol_handlers, format_handlers) {
            Ok(manifest) => manifest,
            Err(error) => {
                info!("Could not fetch record due to {}", error);
                MirrorManifest::new()
            }
        };
    manifest.add_mirror(mirror.clone());
    push_record_to_url(&url, &manifest, protocol_handlers, format_handlers)?;
    Ok(())
}

pub fn register_maintenance_url_for_mirror(
    mirror_name: &String,
    url: &Url,
    registry: &mut MirrorRegistry,
) -> Result<()> {
    let mirror = registry
        .get_mut_mirror_by_name(mirror_name)
        .expect("Could not find mirror!");
    mirror.register_maintenance_url(url.clone());
    Ok(())
}

pub fn sync_mirror_manifest_to_mirror_directory(
    mirror_name: &String,
    registry: &MirrorRegistry,
    base_directory: &PathBuf,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<()> {
    let default_url = generate_url_for_mirror_name(base_directory, mirror_name)?;
    sync_mirror_manifest_for_mirror_to_url(
        &mirror_name,
        &default_url,
        &registry,
        protocol_handlers,
        format_handlers,
    )?;
    Ok(())
}
