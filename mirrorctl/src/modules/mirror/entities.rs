use keshvar::Alpha3;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::hash::{Hash, Hasher};
use std::net::IpAddr;
use url::Url;

#[derive(Default, Clone, PartialEq, Debug, Serialize, Deserialize)]
pub enum CountryCodeOrLocal {
    #[default]
    Local,
    Alpha3(Alpha3),
}

#[derive(Default, Hash, Eq, Clone, PartialEq, Debug, Serialize, Deserialize)]
enum MirrorTier {
    Source = 0,
    Distribution = 1,
    CDN = 2,
    Edge = 3,
    #[default]
    Local = 4,
}

#[derive(Hash, Eq, Clone, PartialEq, Debug, Serialize, Deserialize)]
enum UpstreamReference {
    Mirror,
    String,
}

#[derive(Hash, Eq, Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct MirrorOperator {
    username: String,
    id: u32,
    public_email: String,
}

#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct Mirror {
    pub name: String,
    pub owner: Option<MirrorOperator>,
    pub country: Option<CountryCodeOrLocal>,
    tier: MirrorTier,
    upstream: Option<UpstreamReference>,
    bandwith: f32,
    visible: bool,
    ips: HashSet<IpAddr>,
    maintenance_annoucement_urls: HashSet<Url>,
    urls: HashSet<Url>,
}

impl Mirror {
    pub fn new(name: String) -> Mirror {
        Mirror {
            name: name.clone(),
            owner: None,
            country: None,
            tier: MirrorTier::default(),
            upstream: None,
            bandwith: 0.0,
            visible: false,
            ips: HashSet::default(),
            maintenance_annoucement_urls: HashSet::default(),
            urls: HashSet::default(),
        }
    }

    pub fn register_maintenance_url(&mut self, url: Url) {
        self.maintenance_annoucement_urls.insert(url);
    }

    pub fn get_maintenance_urls(&self) -> &HashSet<Url> {
        &self.maintenance_annoucement_urls
    }
}

impl Hash for Mirror {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}

impl PartialEq for Mirror {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Eq for Mirror {}
