use super::entities::Mirror;
use std::collections::{HashMap, HashSet};
use url::Url;

#[derive(Default, Clone, PartialEq, Debug)]
pub struct MirrorRegistry {
    mirrors_by_name: HashMap<String, Mirror>,
    urls_by_mirror_name: HashMap<String, HashSet<String>>,
    mirror_names_by_url: HashMap<String, HashSet<String>>,
}

impl MirrorRegistry {
    pub fn add_mirror(&mut self, mirror: Mirror) {
        self.mirrors_by_name.insert(mirror.name.clone(), mirror);
    }

    pub fn register_url_for_mirror(&mut self, url: &Url, mirror_name: &String) {
        let url_string: String = String::from(url.clone());

        let mut names = match self.mirror_names_by_url.remove(&url_string) {
            Some(names) => names,
            None => HashSet::default(),
        };
        names.insert(mirror_name.clone());
        self.mirror_names_by_url.insert(url_string.clone(), names);

        let mut urls = match self.urls_by_mirror_name.remove(mirror_name) {
            Some(urls) => urls,
            None => HashSet::default(),
        };
        urls.insert(url_string.clone());
        self.urls_by_mirror_name.insert(mirror_name.clone(), urls);
    }

    pub fn get_mirrors_by_url(&self, url: &Url) -> HashSet<&Mirror> {
        let url_string: String = String::from(url.clone());

        let mirrors = match self.mirror_names_by_url.get(&url_string) {
            Some(names) => {
                let mut mirrors = HashSet::default();
                for name in names {
                    let Some(mirror) = self.mirrors_by_name.get(name) else {
                        continue;
                    };
                    mirrors.insert(mirror);
                }
                mirrors
            }
            None => HashSet::default(),
        };
        mirrors
    }

    pub fn get_mirror_by_name(&self, mirror_name: &String) -> Option<&Mirror> {
        self.mirrors_by_name.get(mirror_name)
    }

    pub fn get_mut_mirror_by_name(&mut self, mirror_name: &String) -> Option<&mut Mirror> {
        self.mirrors_by_name.get_mut(mirror_name)
    }

    pub fn get_or_insert(&mut self, mirror_name: &String, mirror: Mirror) -> &Mirror {
        let registered_mirror = self
            .mirrors_by_name
            .entry(mirror_name.clone())
            .or_insert(mirror);
        registered_mirror
    }

    pub fn extract_mirror(&mut self, mirror_name: &String) -> Option<Mirror> {
        self.mirrors_by_name.remove(mirror_name)
    }

    pub fn get_known_urls(&self) -> HashSet<Url> {
        let mut urls: HashSet<Url> = HashSet::default();
        for url_string in self.mirror_names_by_url.keys() {
            let Ok(url) = Url::try_from(url_string.as_str()) else {
                continue;
            };
            urls.insert(url);
        }
        urls
    }

    pub fn get_knwon_mirror_names(&self) -> HashSet<&String> {
        let mut names: HashSet<&String> = HashSet::default();
        for name in self.mirrors_by_name.keys() {
            names.insert(name);
        }
        names
    }
}
