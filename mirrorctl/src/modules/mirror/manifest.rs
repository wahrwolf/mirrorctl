use super::entities::Mirror;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::collections::{hash_map::IntoIter as HashMapIter, HashMap};

#[derive(Default, Eq, Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct MirrorManifest {
    mirrors: HashMap<String, Mirror>,
    last_updated: DateTime<Utc>,
}

impl MirrorManifest {
    pub fn new() -> MirrorManifest {
        MirrorManifest {
            mirrors: HashMap::default(),
            last_updated: Utc::now(),
        }
    }

    pub fn add_mirror(&mut self, mirror: Mirror) {
        self.mirrors.insert(mirror.name.clone(), mirror);
    }
}

impl IntoIterator for MirrorManifest {
    type Item = (String, Mirror);
    type IntoIter = HashMapIter<String, Mirror>;

    fn into_iter(self) -> Self::IntoIter {
        self.mirrors.into_iter()
    }
}

impl FromIterator<Mirror> for MirrorManifest {
    fn from_iter<I: IntoIterator<Item = Mirror>>(iter: I) -> Self {
        let mut manifest = MirrorManifest {
            mirrors: HashMap::default(),
            last_updated: Utc::now(),
        };
        for mirror in iter {
            manifest.add_mirror(mirror);
        }
        manifest
    }
}
