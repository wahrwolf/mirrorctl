use clap::Parser;
use log::{error, Level};
use std::process;

use simple_logger::init_with_level;

mod cli;
use cli::{route_subcommand, CommandGroup, ProgramState};

pub mod external_fascade;
pub mod modules;
pub mod url_handler;

#[derive(Debug, Parser)] // requires `derive` feature
#[command(name = "mirrorctl")]
#[command(about = "A cool tool to help mirror operators", long_about = None)]
struct Cli {
    #[command(subcommand)]
    group: CommandGroup,
    #[arg(short, long)]
    log_level: Option<Level>,
}

fn main() {
    let args = Cli::parse();
    let _ = match args.log_level {
        Some(log_level) => init_with_level(log_level),
        None => init_with_level(Level::Error),
    };

    let state = match ProgramState::init() {
        Err(err) => {
            error!("Failed due to {err:#}");
            process::exit(1);
        }
        Ok(state) => state,
    };

    if let Err(err) = route_subcommand(args.group, state) {
        error!("Failed due to {err:#}");
        process::exit(1);
    }
}
