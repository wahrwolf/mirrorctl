use crate::modules::mirror::Mirror;
use dirs::{config_dir, data_dir};
use std::path::{Path, PathBuf};

pub fn get_xdg_base_directory() -> PathBuf {
    let xdg_data_dir = match data_dir() {
        Some(dir) => dir,
        None => Path::new(".").to_path_buf(),
    };

    let base_dir: PathBuf = xdg_data_dir.join("mirrorctl");
    base_dir
}

pub fn get_xdg_config_directory() -> PathBuf {
    let xdg_config_dir = match config_dir() {
        Some(dir) => dir,
        None => Path::new(".").to_path_buf(),
    };

    let base_dir: PathBuf = xdg_config_dir.join("mirrorctl");
    base_dir
}

pub fn get_default_config_file(config_directory: PathBuf) -> PathBuf {
    config_directory.join("config.toml")
}

pub fn get_mirror_directory(base_directory: &PathBuf) -> PathBuf {
    let mirror_directory: PathBuf = base_directory.join("mirrors");
    mirror_directory
}

pub fn get_mirror_directory_by_name(base_directory: &PathBuf, mirror_name: &String) -> PathBuf {
    let mirror_directory: PathBuf = get_mirror_directory(base_directory);
    mirror_directory.join(mirror_name)
}

pub fn get_mirror_directory_by_mirror(base_directory: &PathBuf, mirror: &Mirror) -> PathBuf {
    get_mirror_directory_by_name(base_directory, &mirror.name)
}
