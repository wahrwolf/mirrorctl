use super::config::Config;
use super::dirs::{
    get_default_config_file, get_mirror_directory, get_xdg_base_directory, get_xdg_config_directory,
};
use super::registries::{
    initialize_format_handler_registry, initialize_maintenance_manifest_registry,
    initialize_mirror_manifest_registry, initialize_protocol_handler_registry,
};
use crate::modules::maintenance::MaintenanceEventRegistry;
use crate::modules::mirror::MirrorRegistry;
use crate::url_handler::{FormatHandlerRegistry, ProtocolHandlerRegistry};
use anyhow::Result;
use std::path::PathBuf;

pub struct ProgramState {
    pub config: Config,
    pub format_handlers: FormatHandlerRegistry,
    pub protocol_handlers: ProtocolHandlerRegistry,
    pub mirrors: MirrorRegistry,
    pub events: MaintenanceEventRegistry,
    pub base_directory: PathBuf,
    pub config_directory: PathBuf,
    pub mirror_directory: PathBuf,
}

impl ProgramState {
    pub fn init() -> Result<Self> {
        let base_directory = get_xdg_base_directory();
        let config_directory = get_xdg_config_directory();
        let default_config_file = get_default_config_file(config_directory.clone());
        let config = match Config::from_file(&default_config_file) {
            Ok(config) => config,
            Err(error) => {
                println!("{:?}", error);
                Config::default()
            }
        };
        let format_handlers = initialize_format_handler_registry(&config)?;
        let protocol_handlers = initialize_protocol_handler_registry(&config)?;
        let mirror_directory = get_mirror_directory(&base_directory);

        let mirrors = initialize_mirror_manifest_registry(
            &mirror_directory,
            &protocol_handlers,
            &format_handlers,
        )?;
        let events = initialize_maintenance_manifest_registry(
            &mirror_directory,
            &protocol_handlers,
            &format_handlers,
        )?;

        let state = ProgramState {
            config: config,
            config_directory: config_directory,
            format_handlers: format_handlers,
            protocol_handlers: protocol_handlers,
            mirrors: mirrors,
            events: events,
            base_directory: base_directory,
            mirror_directory: mirror_directory,
        };
        Ok(state)
    }
}
