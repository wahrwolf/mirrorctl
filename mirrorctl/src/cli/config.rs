use crate::url_handler::{
    build_record_from_url, try_build_url_from_path_buf, FormatHandlerRegistry,
    ProtocolHandlerConfig, ProtocolHandlerRegistry,
};
use anyhow::Result;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Config {
    pub base_directory: Option<PathBuf>,
    pub config_directory: Option<PathBuf>,
    pub protocol_handler: ProtocolHandlerConfig,
}

impl Config {
    pub fn from_file(path: &PathBuf) -> Result<Self> {
        let protocol_handlers = ProtocolHandlerRegistry::default();
        let format_handlers = FormatHandlerRegistry::default();
        let url = try_build_url_from_path_buf(&path)?;
        let config: Config = build_record_from_url(&url, &protocol_handlers, &format_handlers)?;
        Ok(config)
    }
}
