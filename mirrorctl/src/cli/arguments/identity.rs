use crate::cli::ProgramState;
use anyhow::Result;
use clap::{Args, Subcommand};

use crate::modules::identity::cli_entrypoint::{identity, IdentityArgs};

#[derive(Debug, Args)]
#[command(args_conflicts_with_subcommands = true)]
pub struct IdentityGroup {
    #[command(subcommand)]
    command: IdentityCommand,
}

#[derive(Debug, Subcommand)]
enum IdentityCommand {
    Build(IdentityArgs),
}

pub fn run_identity_command(group_args: IdentityGroup, _state: ProgramState) -> Result<()> {
    match group_args.command {
        IdentityCommand::Build(args) => identity(args),
    }
}
