use anyhow::Result;
use clap::{Args, Subcommand};

use crate::cli::ProgramState;
use crate::modules::sign::cli_entrypoint::{sign, SignArgs};

#[derive(Debug, Args)]
#[command(args_conflicts_with_subcommands = true)]
pub struct SignGroup {
    #[command(subcommand)]
    command: SignCommand,
}

#[derive(Debug, Subcommand)]
enum SignCommand {
    Sign(SignArgs),
}

pub fn run_sign_command(group_args: SignGroup, _state: ProgramState) -> Result<()> {
    match group_args.command {
        SignCommand::Sign(args) => sign(args),
    }
}
