use anyhow::Result;
use clap::{Args, Subcommand};

use crate::cli::ProgramState;
use crate::modules::maintenance::cli_entrypoint::{
    cat_manifest, cleanup_local_manifests, create_event, create_manifest_for_mirror, delete_event,
    edit_event, edit_manifest, fetch_manifest_from_url, list_events, list_manifests,
    push_manifest_to_url, register_manifest_for_mirror, show_event, sync_manifests, update_event,
    CatManifestArgs, CleanupArgs, CreateEventArgs, CreateManifestArgs, DeleteEventArgs,
    EditEventArgs, EditManifestArgs, FetchManifestArgs, ListEventArgs, ListManifestArgs,
    PushManifestArgs, RegisterManifestArgs, ShowEventArgs, SyncManifestArgs, UpdateEventArgs,
};

#[derive(Debug, Args)]
#[command(args_conflicts_with_subcommands = true)]
pub struct MaintenanceGroup {
    #[command(subcommand)]
    group: MaintenanceCommandGroup,
}

#[derive(Debug, Subcommand)]
enum MaintenanceCommandGroup {
    #[command(subcommand)]
    Event(MaintenanceEventGroup),
    #[command(subcommand)]
    Remote(MaintenanceRemoteGroup),
    #[command(subcommand)]
    Manifest(MaintenanceManifestGroup),
}

#[derive(Debug, Subcommand)]
enum MaintenanceRemoteGroup {
    Add(RegisterManifestArgs),
}

#[derive(Debug, Subcommand)]
enum MaintenanceEventGroup {
    Show(ShowEventArgs),
    Cat(ShowEventArgs),
    List(ListEventArgs),
    Create(CreateEventArgs),
    Schedule(CreateEventArgs),
    Update(UpdateEventArgs),
    Edit(EditEventArgs),
    Delete(DeleteEventArgs),
}

#[derive(Debug, Subcommand)]
enum MaintenanceManifestGroup {
    Cat(CatManifestArgs),
    List(ListManifestArgs),
    Create(CreateManifestArgs),
    Push(PushManifestArgs),
    Edit(EditManifestArgs),
    Fetch(FetchManifestArgs),
    Sync(SyncManifestArgs),
    Cleanup(CleanupArgs),
}

pub fn run_maintenance_command(group_args: MaintenanceGroup, state: ProgramState) -> Result<()> {
    match group_args.group {
        MaintenanceCommandGroup::Event(command) => match command {
            MaintenanceEventGroup::Show(args) | MaintenanceEventGroup::Cat(args) => {
                show_event(args, state)
            }
            MaintenanceEventGroup::List(args) => list_events(args, state),
            MaintenanceEventGroup::Create(args) | MaintenanceEventGroup::Schedule(args) => {
                create_event(args, state)
            }
            MaintenanceEventGroup::Update(args) => update_event(args, state),
            MaintenanceEventGroup::Edit(args) => edit_event(args, state),
            MaintenanceEventGroup::Delete(args) => delete_event(args, state),
        },
        MaintenanceCommandGroup::Manifest(command) => match command {
            MaintenanceManifestGroup::Cat(args) => cat_manifest(args, state),
            MaintenanceManifestGroup::List(args) => list_manifests(args, state),
            MaintenanceManifestGroup::Push(args) => push_manifest_to_url(args, state),
            MaintenanceManifestGroup::Create(args) => create_manifest_for_mirror(args, state),
            MaintenanceManifestGroup::Edit(args) => edit_manifest(args, state),
            MaintenanceManifestGroup::Fetch(args) => fetch_manifest_from_url(args, state),
            MaintenanceManifestGroup::Sync(args) => sync_manifests(args, state),
            MaintenanceManifestGroup::Cleanup(args) => cleanup_local_manifests(args, state),
        },
        MaintenanceCommandGroup::Remote(command) => match command {
            MaintenanceRemoteGroup::Add(args) => register_manifest_for_mirror(args, state),
        },
    }
}
