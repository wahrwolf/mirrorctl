use super::config::Config;
use crate::modules::maintenance::{
    discover_maintenance_events_from_directory, MaintenanceEventRegistry,
};
use crate::modules::mirror::{add_mirrors_from_directory, MirrorRegistry};
use crate::url_handler::{FormatHandlerRegistry, ProtocolHandlerRegistry};
use anyhow::{Context, Result};
use log::debug;
use std::fs::create_dir_all;
use std::path::PathBuf;

pub fn initialize_protocol_handler_registry(config: &Config) -> Result<ProtocolHandlerRegistry> {
    let registry = ProtocolHandlerRegistry::new(&config.protocol_handler);
    Ok(registry)
}

pub fn initialize_format_handler_registry(_config: &Config) -> Result<FormatHandlerRegistry> {
    let registry = FormatHandlerRegistry::default();
    Ok(registry)
}

pub fn initialize_maintenance_manifest_registry(
    mirror_directory: &PathBuf,
    protocol_handler: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<MaintenanceEventRegistry> {
    let mut registry = MaintenanceEventRegistry::default();
    if !mirror_directory.exists() {
        debug!("Did not find default data dir! Creating it!");
        create_dir_all(&mirror_directory)?;
    }
    for dir_entry in mirror_directory.read_dir()? {
        let directory: PathBuf = match dir_entry {
            Ok(file) => {
                let path = file.path();
                if path.is_dir() {
                    path
                } else {
                    debug!("Skipping file {:?}", file);
                    continue;
                }
            }
            _ => continue,
        };
        let mirror_name = directory
            .file_name()
            .context("Could not get filename")?
            .to_str()
            .context("Could not parse to string!")?
            .to_string();
        let Ok(()) = discover_maintenance_events_from_directory(
            &directory,
            Some(&mirror_name),
            &mut registry,
            protocol_handler,
            format_handlers,
        ) else {
            debug!("Skipping directory {:?}", directory);
            continue;
        };
    }
    Ok(registry)
}

pub fn initialize_mirror_manifest_registry(
    mirror_directory: &PathBuf,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<MirrorRegistry> {
    let mut registry = MirrorRegistry::default();
    if !mirror_directory.exists() {
        debug!("Did not find default data dir! Creating it!");
        create_dir_all(&mirror_directory)?;
    }
    for dir_entry in mirror_directory.read_dir()? {
        let directory: PathBuf = match dir_entry {
            Ok(file) => {
                let path = file.path();
                if path.is_dir() {
                    path
                } else {
                    continue;
                }
            }
            _ => continue,
        };
        let Ok(()) = add_mirrors_from_directory(
            &directory,
            &mut registry,
            protocol_handlers,
            format_handlers,
        ) else {
            continue;
        };
    }
    Ok(registry)
}
