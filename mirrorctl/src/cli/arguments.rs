use super::state::ProgramState;
use anyhow::Result;
use clap::Subcommand;

mod identity;
use identity::{run_identity_command, IdentityGroup};

mod sign;
use sign::{run_sign_command, SignGroup};

mod maintenance;
use maintenance::{run_maintenance_command, MaintenanceGroup};

#[derive(Debug, Subcommand)]
pub enum CommandGroup {
    Identity(IdentityGroup),
    Sign(SignGroup),
    Maintenance(MaintenanceGroup),
}

pub fn route_subcommand(group: CommandGroup, state: ProgramState) -> Result<()> {
    match group {
        CommandGroup::Identity(command) => run_identity_command(command, state),
        CommandGroup::Sign(command) => run_sign_command(command, state),
        CommandGroup::Maintenance(command) => run_maintenance_command(command, state),
    }
}
