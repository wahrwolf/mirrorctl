use crate::url_handler::{
    build_record_from_url, fetch_string_from_url, push_record_to_url, push_string_to_url,
    try_build_url_from_path_buf, FormatHandlerRegistry, ProtocolHandlerRegistry,
};
use anyhow::Result;
use serde::{de::DeserializeOwned, Serialize};
use std::path::PathBuf;
use std::process::Command;
use tempfile::TempDir;
use url::Url;

pub fn edit_file(editor: String, target_file: &PathBuf) -> Result<()> {
    Command::new(editor).arg(&target_file).status()?;
    Ok(())
}

pub fn edit_record<T: DeserializeOwned + Serialize>(
    editor: String,
    mut record: T,
    format: String,
    protocol_handlers: &ProtocolHandlerRegistry,
    format_handlers: &FormatHandlerRegistry,
) -> Result<T> {
    let tmp_dir: TempDir = TempDir::new()?;
    let mut target_file = tmp_dir.path().join("event");
    target_file.set_extension(format);
    let url = try_build_url_from_path_buf(&target_file)?;
    push_record_to_url(&url, &record, protocol_handlers, format_handlers)?;
    edit_file(editor, &target_file)?;
    record = build_record_from_url(&url, protocol_handlers, format_handlers)?;
    Ok(record)
}

pub fn edit_record_from_url(
    editor: String,
    url: &Url,
    format: String,
    protocol_handlers: &ProtocolHandlerRegistry,
) -> Result<()> {
    let mut record = match fetch_string_from_url(url, protocol_handlers)? {
        Some(record) => record,
        None => "".to_string(),
    };

    let tmp_dir: TempDir = TempDir::new()?;
    let mut target_file = tmp_dir.path().join("event");
    target_file.set_extension(format);
    let target_url = try_build_url_from_path_buf(&target_file)?;
    push_string_to_url(&target_url, &record, protocol_handlers)?;

    edit_file(editor, &target_file)?;

    record = match fetch_string_from_url(&target_url, protocol_handlers)? {
        Some(record) => record,
        None => "".to_string(),
    };
    push_string_to_url(&url, &record, protocol_handlers)?;

    Ok(())
}
