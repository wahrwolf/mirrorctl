use super::*;
use crate::url_handler::try_build_url_from_path_buf;
use tempfile::TempDir;

#[test]
fn struct_can_be_stored_in_toml() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let target_file = tmp_dir.path().join("test.toml");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let good_record = TestStruct::build_foo();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: TestStruct = build_record_from_url(&url, &protocol_handlers, &format_handlers)
        .expect("Could not parse record");

    assert_eq!(good_record, candidate.clone());

    let bad_record = TestStruct::build_bar();
    assert_ne!(bad_record, candidate);
}

#[test]
fn struct_can_be_stored_in_json() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let target_file = tmp_dir.path().join("test.json");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let good_record = TestStruct::build_foo();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: TestStruct = build_record_from_url(&url, &protocol_handlers, &format_handlers)
        .expect("Could not parse record");

    assert_eq!(good_record, candidate.clone());

    let bad_record = TestStruct::build_bar();
    assert_ne!(bad_record, candidate);
}

#[test]
fn nested_struct_without_items_can_be_stored_in_toml() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let target_file = tmp_dir.path().join("test.toml");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let good_record = NestedStruct::build_struct_without_items();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: NestedStruct = build_record_from_url(&url, &protocol_handlers, &format_handlers)
        .expect("Could not parse record");

    assert_eq!(good_record, candidate.clone());

    let bad_record = NestedStruct::build_struct_with_items();
    assert_ne!(bad_record, candidate);
}

#[test]
fn nested_struct_without_items_can_be_stored_in_json() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let target_file = tmp_dir.path().join("test.json");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let good_record = NestedStruct::build_struct_without_items();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: NestedStruct = build_record_from_url(&url, &protocol_handlers, &format_handlers)
        .expect("Could not parse record");

    assert_eq!(good_record, candidate.clone());

    let bad_record = NestedStruct::build_struct_with_items();
    assert_ne!(bad_record, candidate);
}

#[test]
fn nested_struct_with_items_can_be_stored_in_json() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let target_file = tmp_dir.path().join("test.json");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let good_record = NestedStruct::build_struct_with_items();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: NestedStruct = build_record_from_url(&url, &protocol_handlers, &format_handlers)
        .expect("Could not parse record");

    assert_eq!(good_record, candidate.clone());

    let bad_record = NestedStruct::build_struct_without_items();
    assert_ne!(bad_record, candidate);
}

#[test]
fn nested_struct_with_items_can_be_stored_in_toml() {
    let tmp_dir: TempDir = TempDir::new().expect("Could not create TempDir");
    let target_file = tmp_dir.path().join("test.toml");
    let url = try_build_url_from_path_buf(&target_file).expect("Could not build url");
    let format_handlers = FormatHandlerRegistry::default();
    let protocol_handlers = ProtocolHandlerRegistry::default();
    let good_record = NestedStruct::build_struct_with_items();
    push_record_to_url(&url, &good_record, &protocol_handlers, &format_handlers)
        .expect("Could not push record");

    let candidate: NestedStruct = build_record_from_url(&url, &protocol_handlers, &format_handlers)
        .expect("Could not parse record");

    assert_eq!(good_record, candidate.clone());

    let bad_record = NestedStruct::build_struct_without_items();
    assert_ne!(bad_record, candidate);
}
